package bryn.com.githubclient.interactor;

import com.squareup.okhttp.Credentials;

import javax.inject.Inject;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.utilities.SchedulersFactory;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Func1;

public class LoginInteractor {

    private final GithubApi api;
    private final SharedPreferencesUtil sharedPreferences;
    private final SchedulersFactory schedulersFactory;

    @Inject
    public LoginInteractor(GithubApi api,
                           SharedPreferencesUtil util,
                           SchedulersFactory schedulersFactory) {
        this.api = api;
        this.sharedPreferences = util;
        this.schedulersFactory = schedulersFactory;
    }


    public Observable<Void> login(final LoginData loginData) {
        return Observable.just(loginData)
                .flatMap(login)
                .doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        sharedPreferences.saveCredentials(loginData);
                    }
                })
                .subscribeOn(schedulersFactory.getIOScheduler())
                .observeOn(schedulersFactory.getMainThreadScheduler())
                .cache(1);
    }

    private final Func1<LoginData, Observable<Void>> login = new Func1<LoginData, Observable<Void>>() {
        @Override
        public Observable<Void> call(LoginData loginData) {
            if (loginData.tfaCode == null || loginData.tfaCode.length() == 0) {
                return api.login(getBasicAuthHeader(loginData));
            } else {
                return api.login(getBasicAuthHeader(loginData), loginData.tfaCode);
            }
        }
    };

    private String getBasicAuthHeader(LoginData loginData) {
        return Credentials.basic(loginData.login, loginData.password);
    }
}
