package bryn.com.githubclient.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.persistence.DataRemover;

@Singleton
public class SharedPreferencesUtil implements DataRemover {

    private static final String LOGIN_KEY = "LOGIN";
    private static final String PASSWORD_KEY = "PASSWORD";
    private static final String LAST_SEARCH_WORD_KEY = "LAST_SEARCH_WORD_KEY";
    private static final String LAST_SEARCH_DOWNLOADED_PAGE_KEY = "LAST_SEARCH_DOWNLOADED_PAGE_KEY";
    private static final String LAST_MAX_SEARCH_PAGE_KEY = "LAST_MAX_SEARCH_PAGE_KEY";
    private final SharedPreferences preferences;

    @Inject
    public SharedPreferencesUtil(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    // insecure
    public void saveCredentials(LoginData loginData) {
        preferences.edit().putString(LOGIN_KEY, loginData.login).apply();
        preferences.edit().putString(PASSWORD_KEY, loginData.password).apply();
    }


    public LoginData getCredentials() {
        String login = preferences.getString(LOGIN_KEY, null);
        String password = preferences.getString(PASSWORD_KEY, null);

        if (login == null || password == null) {
            return null;
        }

        return new LoginData(login, password);
    }

    public String getLastSearchWord(){
        return preferences.getString(LAST_SEARCH_WORD_KEY, null);
    }

    public void saveLastSearchWord(String word){
        preferences.edit().putString(LAST_SEARCH_WORD_KEY, word).apply();
    }

    public int getLastSearchDownloadedPage(){
        return preferences.getInt(LAST_SEARCH_DOWNLOADED_PAGE_KEY,-1);
    }

    public void saveLastSearchDownloadedPage(int page){
        preferences.edit().putInt(LAST_SEARCH_DOWNLOADED_PAGE_KEY, page).apply();
    }

    public int getLastMaxSearchPage(){
        return preferences.getInt(LAST_MAX_SEARCH_PAGE_KEY,-1);
    }

    public void saveLastMaxSearchPage(int page){
        preferences.edit().putInt(LAST_MAX_SEARCH_PAGE_KEY, page).apply();
    }

    @Override
    public void removeData() {
        preferences.edit().clear().apply();
    }
}
