package bryn.com.githubclient.espresso;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import bryn.com.githubclient.MockHttpError;
import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.interactor.LoginInteractor;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Func1;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * Created by pawelbryniarski on 19.12.15.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginScreenTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void testInitialLoginScreen() {
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()));
        onView(withId(R.id.login)).check(matches(isDisplayed()));
        onView(withId(R.id.password)).check(matches(isDisplayed()));
        onView(withId(R.id.login)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
        onView(withId(R.id.tfa_code)).check(matches(not(isDisplayed())));
        onView(withId(R.id.login_button)).check(matches(isDisplayed()));
    }

    @Test
    public void testLoginFailure() {
        LoginInteractor.mockLogin = Observable.just("").map(new Func1<String, Void>() {
            @Override
            public Void call(String s) {
                throw Exceptions.propagate(new IOException());
            }
        });
        onView(withId(R.id.login)).perform(replaceText("bryn2"));
        onView(withId(R.id.password)).perform(replaceText("123Test"));
        onView(withId(R.id.login_button)).perform(click());
        onView(withText(R.string.login_failed)).check(matches(isDisplayed()));
        onView(withId(R.id.tfa_code)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testEmptyFields() throws Exception {
        onView(withId(R.id.login)).perform(replaceText(""));
        onView(withId(R.id.login)).perform(replaceText(""));
        onView(withId(R.id.login_button)).perform(click());
        onView(withText(R.string.error_login_fields)).check(matches(isDisplayed()));
        onView(withId(R.id.tfa_code)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testLoginSuccessful() throws Exception {
        LoginInteractor.mockLogin = Observable.just("").map(new Func1<String, Void>() {
            @Override
            public Void call(String s) {
                return null;
            }
        });
        onView(withId(R.id.login)).perform(replaceText("cokolwiek"));
        onView(withId(R.id.password)).perform(replaceText("cokolwiek"));
        onView(withId(R.id.login_button)).perform(click());
        onView(withId(R.id.fragment_repositories)).check(matches(isDisplayed()));
    }


    @Test
    public void testTfaRequired() throws Exception {
        LoginInteractor.mockLogin = Observable.error(MockHttpError.getException(401, true))
                .map(new Func1<Object, Void>() {
                    @Override
                    public Void call(Object o) {
                        return null;
                    }
                });
        onView(withId(R.id.login)).perform(replaceText("cokolwiek"));
        onView(withId(R.id.password)).perform(replaceText("cokolwiek"));
        onView(withId(R.id.login_button)).perform(click());

        onView(withId(R.id.tfa_code)).check(matches(isDisplayed()));
    }
}
