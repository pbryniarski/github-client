package bryn.com.githubclient;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.LinkedList;
import java.util.List;

import bryn.com.githubclient.dataModel.Owner;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.persistence.db.UserRepositoriesDbHelper;
import bryn.com.githubclient.persistence.db.UserRepositoriesStorage;

import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class UserRepositoriesStorageTest {


    private UserRepositoriesStorage storage;
    private Repository repository;
    private List<Repository> repositories = new LinkedList<>();

    @Before
    public void setUp() {

        Context context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "TEST");
        UserRepositoriesDbHelper dbHelper = new UserRepositoriesDbHelper(context);
        storage = new UserRepositoriesStorage(dbHelper);

        Owner owner = new Owner("login", "avatar");
        repository = new Repository(1, "name", "fullName", "description", "3", "2", "master", owner);
        repositories.add(repository);
        repositories.add(new Repository(2, "name", "fullName", "description", "3", "2", "master", owner));
        repositories.add(new Repository(3, "name", "fullName", "description", "3", "2", "master", owner));
        repositories.add(new Repository(4, "name", "fullName", "description", "3", "2", "master", owner));
        repositories.add(new Repository(5, "name", "fullName", "description", "3", "2", "master", owner));
    }

    @After
    public void tearDown() throws Exception {
        storage.clearAllRepositories();
    }


    @Test
    public void testSaveAndClear() {
        storage.saveRepositories(repositories);

        List<Repository> repositoryList = storage.readRepositories();

        assertEquals(1, repositoryList.size());

        storage.clearAllRepositories();

        repositoryList = storage.readRepositories();

        assertEquals(0, repositoryList.size());
    }

    @Test
    public void testSaveManyRepositories() {
        storage.saveRepositories(repositories);
        List<Repository> readRepositories = storage.readRepositories();
        assertEquals(readRepositories, readRepositories);
    }


}

