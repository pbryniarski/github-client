package bryn.com.githubclient;


import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response.Builder;

import retrofit.HttpException;
import retrofit.Response;
/**
 * Created by pawelbryniarski on 19.12.15.
 */
public class MockHttpError {

    public static HttpException getException(int code, boolean tfaHeader) {
        Request request = new com.squareup.okhttp.Request.Builder()
                .url("http://www.onet.pl")
                .build();

        Builder builder = new Builder();
        if (tfaHeader) {
            builder.header("X-GitHub-OTP", "required ; app");
        } else {
            builder.header("Header", "required ; app");
        }
        builder.code(code);
        builder.protocol(Protocol.HTTP_1_0);
        builder.request(request);
        com.squareup.okhttp.Response response = builder.build();

        Response retrofitResponse = Response.error(null, response);
        return new HttpException(retrofitResponse);
    }
}
