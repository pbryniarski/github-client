package bryn.com.githubclient.interactor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import bryn.com.githubclient.interactor.repositories.PageNumberParser;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 10.12.15.
 */
@RunWith(Parameterized.class)
public class LastPageNumberParserTest {

    private static final String nextPageNumber2WithLastNext = "<https://api.github.com/user/82592/repos?page=2>; rel=\"next\", <https://api.github.com/user/82592/repos?page=6>; rel=\"last\"";
    private static final String nextPageNumber3WithLastFirstPrev = "<https://api.github.com/user/82592/repos?page=3>; rel=\"next\", <https://api.github.com/user/82592/repos?page=6>; rel=\"last\", <https://api.github.com/user/82592/repos?page=1>; rel=\"first\", <https://api.github.com/user/82592/repos?page=1>; rel=\"prev\"";
    private static final String nextPageNumberDoesntExist = "<https://api.github.com/user/82592/repos?page=1&q=e&a=2>; rel=\"first\", <https://api.github.com/user/82592/repos?page=5>; rel=\"prev\"";

    @Parameterized.Parameters
    public static Collection headersToNextPageNumbers() {
        return Arrays.asList(new Object[][]{
                {nextPageNumber2WithLastNext, 6},
                {nextPageNumber3WithLastFirstPrev, 6},
                {nextPageNumberDoesntExist, -1},
                {null, -1},
                {"", -1}
        });
    }

    private final String linkHeader;
    private final int nextPageNumber;

    public LastPageNumberParserTest(String linkHeader, int nextPageNumber) {
        this.linkHeader = linkHeader;
        this.nextPageNumber = nextPageNumber;
    }


    @Test
    public void testParse() throws Exception {
        PageNumberParser pageNumberParser = new PageNumberParser();
        assertEquals("Test for link: " + linkHeader, nextPageNumber, pageNumberParser.getLastPageNumber(linkHeader));
    }
}