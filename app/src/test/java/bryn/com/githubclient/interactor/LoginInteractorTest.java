package bryn.com.githubclient.interactor;

import com.squareup.okhttp.Credentials;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.utilities.SchedulersFactory;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;
import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by pawelbryniarski on 19.12.15.
 */
public class LoginInteractorTest {

    @Mock
    SharedPreferencesUtil sharedPreferencesUtil;

    @Mock
    GithubApi api;

    @Mock
    SchedulersFactory schedulersFactory;


    Observable<Void> observable = Observable.just(null);

    private LoginInteractor interactor;
    private TestSubscriber<Void> testSubscriber;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        interactor = new LoginInteractor(api, sharedPreferencesUtil, schedulersFactory);
        testSubscriber = new TestSubscriber<>();

        doReturn(observable).when(api).login(anyString());
        doReturn(observable).when(api).login(anyString(), anyString());
        doReturn(Schedulers.immediate()).when(schedulersFactory).getIOScheduler();
        doReturn(Schedulers.immediate()).when(schedulersFactory).getMainThreadScheduler();
    }

    @Test
    public void testLoginTfa() throws Exception {
        LoginData loginData = new LoginData("a", "b", "1");
        interactor
                .login(loginData)
                .subscribe(testSubscriber);

        verify(api).login(
                eq(Credentials.basic(loginData.login, loginData.password)),
                eq(loginData.tfaCode));
    }


    @Test
    public void testLoginNonTfa() throws Exception {
        LoginData loginData = new LoginData("a", "b");
        interactor
                .login(loginData)
                .subscribe(testSubscriber);

        verify(api).login(
                eq(Credentials.basic(loginData.login, loginData.password)));
    }

    @Test
    public void testCredentialsSavedOnSuccess() {
        LoginData loginData = new LoginData("a", "b");
        interactor
                .login(loginData)
                .subscribe(testSubscriber);

        verify(sharedPreferencesUtil).saveCredentials(eq(loginData));
    }

    @Test
    public void testCredentialsNotSavedOnError() {

        doReturn(Observable.error(new IOException())).when(api).login(anyString());
        LoginData loginData = new LoginData("a", "b");
        interactor
                .login(loginData)
                .subscribe(testSubscriber);

        verify(sharedPreferencesUtil, times(0)).saveCredentials(eq(loginData));
    }
}
