package bryn.com.githubclient;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import bryn.com.githubclient.persistence.file.RepoSearchWordsStorage;
import bryn.com.githubclient.persistence.file.StringsToFilePersistence;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class RepoSearchWordsStorageTest {

    private RepoSearchWordsStorage storage;
    private List<String> words;

    @Before
    public void setUp() throws IOException {
        Context context = mock(Context.class);
        TemporaryFolder temporaryFolder = new TemporaryFolder();
        temporaryFolder.create();
        doReturn(temporaryFolder.newFolder()).when(context).getFilesDir();
        StringsToFilePersistence stringsToFilePersistence = new StringsToFilePersistence(context);
        stringsToFilePersistence.setFileName("temp.txt");
        storage = new RepoSearchWordsStorage(stringsToFilePersistence);

        words = Arrays.asList("retrofit", "tape", "sqlbrite");
        for (String word : words) {
            storage.addWord(word);
        }
    }

    @Test
    public void testSaveRead() throws Exception {
        List<String> read = storage.readWords();
        assertEquals(words, read);
    }

    @Test
    public void testDeleteWords() throws Exception {
        storage.removeData();
        assertEquals(0, storage.readWords().size());
    }
}