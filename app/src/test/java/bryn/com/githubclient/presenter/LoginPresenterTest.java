package bryn.com.githubclient.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import bryn.com.githubclient.MockHttpError;
import bryn.com.githubclient.NavigationController;
import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.fragment.LoginFragment;
import bryn.com.githubclient.fragment.WorkerFragment;
import bryn.com.githubclient.interactor.LoginInteractor;
import rx.Observable;
import rx.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class LoginPresenterTest {


    private Observable<Void> loginObservable = Observable.just(null);

    @Mock
    LoginInteractor interactor;

    @Mock
    LoginFragment loginFragment;

    @Mock
    NavigationController navigationController;

    @Captor
    ArgumentCaptor<LoginData> loginDataArgumentCaptor;
    private LoginPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        doReturn(loginObservable).when(interactor).login(any(LoginData.class));
        presenter = new LoginPresenter(interactor, navigationController);
        presenter.setFragment(loginFragment);
    }

    @Test
    public void testLogin() throws Exception {
        assertNotNull(presenter.login("login", "password"));

        verify(loginFragment).onLoginStart();
        verify(interactor).login(loginDataArgumentCaptor.capture());
        assertEquals("login", loginDataArgumentCaptor.getValue().login);
        assertEquals("password", loginDataArgumentCaptor.getValue().password);
    }

    @Test
    public void testLoginWithTfa() throws Exception {
        assertNotNull(presenter.login("login", "password", "123"));

        verify(loginFragment).onLoginStart();
        verify(interactor).login(loginDataArgumentCaptor.capture());
        assertEquals("login", loginDataArgumentCaptor.getValue().login);
        assertEquals("password", loginDataArgumentCaptor.getValue().password);
        assertEquals("123", loginDataArgumentCaptor.getValue().tfaCode);
    }

    @Test
    public void testLoginSuccess() throws Exception {
        presenter.loginSuccess.call(null);
        verify(loginFragment).onLoginStop();
        verify(navigationController).userLoggedIn();
        assertNull(presenter.loginObservable);
    }

    @Test
    public void testSaveLoggingStateWhenLogging() throws Exception {
        presenter.loginObservable = Observable.just(null);
        WorkerFragment fragment = new WorkerFragment();

        presenter.saveLoggingState(fragment);

        assertEquals(presenter.loginObservable, fragment.loginObservable);
    }

    @Test
    public void testDontSaveLoggingStateWhenNotLogging() throws Exception {
        presenter.loginObservable = null;
        WorkerFragment fragment = new WorkerFragment();

        presenter.saveLoggingState(fragment);

        assertNull(fragment.loginObservable);
    }


    @Test
    public void testRestoreLoggingStateWhenLogging() throws Exception {
        WorkerFragment fragment = new WorkerFragment();
        PublishSubject<Void> ps = PublishSubject.create();
        fragment.loginObservable = ps;

        assertNotNull(presenter.restoreLoggingState(fragment));
        verify(loginFragment).onLoginStart();
        assertNull(fragment.loginObservable);
        assertTrue(ps.hasObservers());
    }

    @Test
    public void testDontRestoreLoggingStateWhenNotLogging() throws Exception {
        WorkerFragment fragment = new WorkerFragment();

        assertNull(presenter.restoreLoggingState(fragment));
        verify(loginFragment, times(0)).onLoginStart();
    }

    @Test
    public void testLoginErrorIOException() throws Exception {
        presenter.loginError.call(new IOException());
        verify(loginFragment).onLoginFailure();
        verify(loginFragment).onLoginStop();
        assertNull(presenter.loginObservable);
        verify(navigationController, times(0)).userLoggedIn();
    }

    @Test
    public void testRetrofitErrorNoTfa() throws Exception {
        presenter.loginError.call(MockHttpError.getException(123, false));

        verify(loginFragment).onLoginFailure();
        assertNull(presenter.loginObservable);
        verify(navigationController, times(0)).userLoggedIn();
    }

    @Test
    public void testRetrofitErrorTfa() throws Exception {
        presenter.loginError.call(MockHttpError.getException(401, true));

        verify(loginFragment).onTfaFailure();
        verify(navigationController, times(0)).userLoggedIn();
        assertNull(presenter.loginObservable);
    }
}
