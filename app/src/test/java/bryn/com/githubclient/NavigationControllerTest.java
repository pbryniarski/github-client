package bryn.com.githubclient;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.fragment.LoginFragment;
import bryn.com.githubclient.fragment.UserRepositoriesFragment;
import bryn.com.githubclient.persistence.OnLogoutDataRemover;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class NavigationControllerTest {

    @Mock
    FragmentManager fragmentManager;

    @Mock
    SharedPreferencesUtil sharedPreferences;

    @Mock
    FragmentTransaction transaction;

    @Mock
    OnLogoutDataRemover remover;

    @Captor
    ArgumentCaptor<Fragment> fragmentCaptor;

    private NavigationController navigationController;

    @SuppressLint("CommitTransaction")
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        doReturn(transaction).when(fragmentManager).beginTransaction();
        doReturn(transaction).when(transaction).replace(anyInt(), any(Fragment.class));
        doReturn(transaction).when(transaction).add(anyInt(), any(Fragment.class));
        doReturn(transaction).when(transaction).addToBackStack(anyString());
        navigationController = new NavigationController(fragmentManager, sharedPreferences, remover);
    }

    @SuppressLint("CommitTransaction")
    @Test
    public void testShowRepoFragmentWhenLoggedIn() throws Exception {
        navigationController.userLoggedIn();

        verify(fragmentManager).beginTransaction();
        verify(transaction).replace(eq(R.id.fragment_container), fragmentCaptor.capture());
        assertTrue(fragmentCaptor.getValue() instanceof UserRepositoriesFragment);
        verify(transaction).commit();
    }

    @SuppressLint("CommitTransaction")
    @Test
    public void testShowInitialScreenWhenNoCredentials() throws Exception {
        doReturn(null).when(sharedPreferences).getCredentials();

        navigationController.showInitialScreen();
        verify(fragmentManager).beginTransaction();
        verify(transaction).replace(eq(R.id.fragment_container), fragmentCaptor.capture());
        assertTrue(fragmentCaptor.getValue() instanceof LoginFragment);
        verify(transaction).commit();
    }

    @SuppressLint("CommitTransaction")
    @Test
    public void testShowInitialScreenWhenCredentialsPresent() {
        doReturn(mock(LoginData.class)).when(sharedPreferences).getCredentials();

        navigationController.showInitialScreen();
        verify(fragmentManager).beginTransaction();
        verify(transaction).replace(eq(R.id.fragment_container), fragmentCaptor.capture());
        assertTrue(fragmentCaptor.getValue() instanceof UserRepositoriesFragment);
        verify(transaction).commit();
    }

    @Test
    public void testOnLogoutDataCleared(){
        navigationController.logout();

        verify(remover).removeData();
    }
}
