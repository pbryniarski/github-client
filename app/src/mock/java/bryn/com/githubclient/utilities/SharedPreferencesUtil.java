package bryn.com.githubclient.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.persistence.DataRemover;

@Singleton
public class SharedPreferencesUtil implements DataRemover {

    private static final String LAST_SEARCH_WORD_KEY = "LAST_SEARCH_WORD_KEY";
    private static final String LAST_SEARCH_DOWNLOADED_PAGE_KEY = "LAST_SEARCH_DOWNLOADED_PAGE_KEY";
    private static final String LAST_MAX_SEARCH_PAGE_KEY = "LAST_MAX_SEARCH_PAGE_KEY";
    public static LoginData loginData;

    private final SharedPreferences preferences;

    @Inject
    public SharedPreferencesUtil(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveCredentials(LoginData loginData) {
    }

    public LoginData getCredentials() {
        return loginData;
    }

    public String getLastSearchWord(){
        return preferences.getString(LAST_SEARCH_WORD_KEY, null);
    }

    public void saveLastSearchWord(String word){
        preferences.edit().putString(LAST_SEARCH_WORD_KEY, word).apply();
    }

    public int getLastSearchDownloadedPage(){
        return preferences.getInt(LAST_SEARCH_DOWNLOADED_PAGE_KEY,-1);
    }

    public void saveLastSearchDownloadedPage(int page){
        preferences.edit().putInt(LAST_SEARCH_DOWNLOADED_PAGE_KEY, page).apply();
    }

    public int getLastMaxSearchPage(){
        return preferences.getInt(LAST_MAX_SEARCH_PAGE_KEY,-1);
    }

    public void saveLastMaxSearchPage(int page){
        preferences.edit().putInt(LAST_MAX_SEARCH_PAGE_KEY, page).apply();
    }

    @Override
    public void removeData() {
        preferences.edit().clear().apply();
    }
}
