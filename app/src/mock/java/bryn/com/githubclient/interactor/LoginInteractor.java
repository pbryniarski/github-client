package bryn.com.githubclient.interactor;

import javax.inject.Inject;

import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;
import rx.Observable;

public class LoginInteractor {

    private final GithubApi api;
    private final SharedPreferencesUtil sharedPreferences;

    @Inject
    public LoginInteractor(GithubApi api, SharedPreferencesUtil util) {
        this.api = api;
        this.sharedPreferences = util;
    }

    public static Observable<Void> mockLogin;

    public Observable<Void> login(final LoginData loginData) {
        return mockLogin;
    }


}
