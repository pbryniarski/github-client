package bryn.com.githubclient.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bryn.com.githubclient.R;
import bryn.com.githubclient.dataModel.Repository;
import butterknife.Bind;
import butterknife.ButterKnife;

public class RepositoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final LayoutInflater inflater;
    private final View.OnClickListener listener;
    private List<Repository> repositories = new ArrayList<>();
    private final Picasso picasso;
    private final OnRepoClickedListener onRepoClickedListener;

    public RepositoriesAdapter(LayoutInflater inflater, Picasso picasso, OnRepoClickedListener onRepoClickedListener) {
        this.inflater = inflater;
        this.picasso = picasso;
        this.onRepoClickedListener = onRepoClickedListener;
        this.listener = new OnSimpleForecastClickListener();
    }

    public void clearRepos() {
        repositories.clear();
        notifyDataSetChanged();
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public interface OnRepoClickedListener {
        void onClicked(Repository repository);
    }

    public void addRepositories(List<Repository> repositories) {
        this.repositories.addAll(repositories);
        notifyItemRangeInserted(this.repositories.size() - repositories.size(), repositories.size());
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.avatar)
        ImageView repoOwnerAvatar;

        @Bind(R.id.repo_name)
        TextView repoName;

        View wholeView;

        public ViewHolder(View v) {
            super(v);
            wholeView = v;
            ButterKnife.bind(this, v);
            wholeView.setTag(this);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.repository_item, parent, false);
        v.setOnClickListener(listener);
        return new ViewHolder(v);
    }

    public class OnSimpleForecastClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            ViewHolder viewHolder = (ViewHolder) v.getTag();
            if (viewHolder != null) {
                Repository repository = repositories.get(viewHolder.getAdapterPosition());
                onRepoClickedListener.onClicked(repository);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        Repository repository = repositories.get(position);
        vh.repoName.setText(repository.name);
        picasso.load(repository.owner.avatarUrl).into(vh.repoOwnerAvatar);
    }

    @Override
    public int getItemCount() {
        return repositories == null ? 0 : repositories.size();
    }
}
