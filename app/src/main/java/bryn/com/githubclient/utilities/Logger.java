package bryn.com.githubclient.utilities;

import android.util.Log;

public class Logger {

    public final static int DEBUG = 1;
    public final static int VERBOSE = 0;
    public final static int INFO = 2;
    public final static int WARNING = 3;
    public final static int ERROR = 4;
    private static int logLevel;


    public static void setLogLevel(int logLevel) {
        Logger.logLevel = logLevel;
    }

    public static void v(String tag, String message) {
        if (logLevel <= VERBOSE) {
            Log.v(tag, message);
        }
    }

    public static void d(String tag, String message) {
        if (logLevel <= DEBUG) {
            Log.d(tag, message);
        }
    }


    public static void i(String tag, String message) {
        if (logLevel <= INFO) {
            Log.i(tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (logLevel <= WARNING) {
            Log.w(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (logLevel <= ERROR) {
            Log.e(tag, message);
        }
    }

    public static void e(String tag, String message, Throwable throwable) {
        if (logLevel <= ERROR) {
            Log.e(tag, message, throwable);
        }
    }
}


