package bryn.com.githubclient.utilities;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 19.12.15.
 */
@Singleton
public class SchedulersFactory {

    @Inject
    public SchedulersFactory() {
    }

    public Scheduler getIOScheduler(){
        return Schedulers.io();
    }

    public Scheduler getMainThreadScheduler(){
        return AndroidSchedulers.mainThread();
    }
}
