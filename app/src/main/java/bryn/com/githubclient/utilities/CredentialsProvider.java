package bryn.com.githubclient.utilities;

import com.squareup.okhttp.Credentials;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.dataModel.LoginData;

@Singleton
public class CredentialsProvider {

    private final SharedPreferencesUtil sharedPreferences;

    @Inject
    public CredentialsProvider(SharedPreferencesUtil sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public String getBasicAuth() {
        LoginData loginData = sharedPreferences.getCredentials();
        return Credentials.basic(loginData.login, loginData.password);
    }
}
