package bryn.com.githubclient.utilities;

import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.R;

/**
 * Created by pawelbryniarski on 08.12.15.
 */
@Singleton
public class Utils {

    @Inject
    public Utils() {
    }

    public void showSnackBar(View view, String text) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = view.getContext().getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int color = typedValue.data;
        snackbar.getView().setBackgroundColor(color);

        snackbar.show();
    }
}
