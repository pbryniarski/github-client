package bryn.com.githubclient.utilities;

import android.app.Activity;
import android.support.annotation.StringRes;

import javax.inject.Inject;

import bryn.com.githubclient.dagger.MainActivityScope;

/**
 * Created by pawelbryniarski on 08.12.15.
 */

@MainActivityScope
public class ResourcesProvider {
    //activity instead of application context to make sure themes are applied
    private final Activity activity;

    @Inject
    public ResourcesProvider(Activity activity) {
        this.activity = activity;
    }

    public String getString(@StringRes int stringRes) {
        return activity.getString(stringRes);
    }

}
