package bryn.com.githubclient.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.inject.Inject;

import bryn.com.githubclient.App;
import bryn.com.githubclient.NavigationController;
import bryn.com.githubclient.R;
import bryn.com.githubclient.dagger.ActivityComponent;
import bryn.com.githubclient.dagger.DaggerActivityComponent;
import bryn.com.githubclient.dagger.MainActivityModule;
import bryn.com.githubclient.fragment.WorkerFragment;
import bryn.com.githubclient.interactor.starring.StarRequestSyncService;
import bryn.com.githubclient.persistence.file.RepoStarRequestStorage;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final String WORKER_FRAGMENT = "WORKER_FRAGMENT_TAG";

    private ActivityComponent activityComponent;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    NavigationController navigationController;

    @Inject
    RepoStarRequestStorage starRequestStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        activityComponent = createActivityComponent();
        activityComponent.inject(this);

        WorkerFragment workerFragment = (WorkerFragment) getFragmentManager().findFragmentByTag(WORKER_FRAGMENT);
        if (workerFragment == null) {
            createWorkerFragment();
        }

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            navigationController.showInitialScreen();
            StarRequestSyncService.executeRequests(this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        starRequestStorage.saveRequests();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void onBackPressed() {
        if (!navigationController.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            navigationController.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ActivityComponent createActivityComponent() {
        return DaggerActivityComponent.builder()
                .appComponent(((App) getApplication()).getAppComponent())
                .mainActivityModule(new MainActivityModule(this, toolbar, getSupportActionBar()))
                .build();
    }

    private void createWorkerFragment() {
        WorkerFragment workerFragment;
        workerFragment = new WorkerFragment();
        getFragmentManager().beginTransaction().add(workerFragment, WORKER_FRAGMENT).commit();
        getFragmentManager().executePendingTransactions();
    }
}
