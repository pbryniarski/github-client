package bryn.com.githubclient;

import android.app.Application;
import android.os.StrictMode;

import bryn.com.githubclient.dagger.AppComponent;
import bryn.com.githubclient.dagger.AppModule;
import bryn.com.githubclient.dagger.DaggerAppComponent;
import bryn.com.githubclient.dagger.NetworkModule;
import bryn.com.githubclient.utilities.Logger;

public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .networkModule(new NetworkModule())
                .appModule(new AppModule(this))
                .build();
        if (BuildConfig.DEBUG) {
            Logger.setLogLevel(Logger.VERBOSE);
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                        .detectAll()
                        .penaltyLog()
                        .build());
                StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectAll()
                        .penaltyLog()
                        .build());

        } else {
            Logger.setLogLevel(Logger.WARNING);
        }
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
