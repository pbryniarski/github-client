package bryn.com.githubclient.persistence.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import bryn.com.githubclient.dataModel.Owner;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.persistence.DataRemover;
import bryn.com.githubclient.utilities.Logger;


public abstract class RepositoriesStorage implements DataRemover {


    private static final String TAG = "UserRepositoriesStorage";
    private final RepositoriesDbHelper helper;


    public RepositoriesStorage(RepositoriesDbHelper helper) {
        this.helper = helper;
    }

    protected abstract String getTableName();

    public static abstract class RepositoryEntry implements BaseColumns {
        public static final String SEARCH_REPOS_TABLE_NAME = "search_repos";
        public static final String USER_REPOS_TABLE_NAME = "user_repos";
        public static final String COLUMN_NAME_REPOSITORY_ID = "repo_id";
        public static final String COLUMN_NAME_REPOSITORY_NAME = "repo_name";
        public static final String COLUMN_NAME_REPOSITORY_FULL_NAME = "repo_full_name";
        public static final String COLUMN_NAME_REPOSITORY_DESCRIPTION = "repo_description";
        public static final String COLUMN_NAME_REPOSITORY_STAR_COUNT = "repo_star_count";
        public static final String COLUMN_NAME_REPOSITORY_WATCH_COUNT = "repo_watch_count";
        public static final String COLUMN_NAME_REPOSITORY_DEFAULT_BRANCH = "repo_default_branch";

        //for simplicity owners will be kept with repo, should be split to a separate table
        public static final String COLUMN_NAME_REPOSITORY_OWNER_LOGIN = "repo_owner_login";
        public static final String COLUMN_NAME_REPOSITORY_AVATAR_URL = "repo_owner_avatar_url";
    }


    public void saveRepositories(List<Repository> repositories) {
        SQLiteDatabase database = helper.getWritableDatabase();
        database.beginTransaction();
        try {
            for (int i = 0; i < repositories.size(); i++) {
                saveRepo(repositories.get(i), database);
            }
            database.setTransactionSuccessful();
        } catch (Exception e) {
            Logger.e(TAG, "Save repositories failed", e);
        } finally {
            database.endTransaction();
        }
    }

    private long saveRepo(Repository repository, SQLiteDatabase database) {
        ContentValues values = getContentValues(repository);

        return database.replace(
                getTableName(),
                null,
                values);
    }

    @NonNull
    private ContentValues getContentValues(Repository repository) {
        ContentValues values = new ContentValues();
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_ID, repository.id);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_NAME, repository.name);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_FULL_NAME, repository.fullName);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_DESCRIPTION, repository.description);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_STAR_COUNT, repository.starsCount);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_WATCH_COUNT, repository.watchersCount);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_DEFAULT_BRANCH, repository.defaultBranch);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_OWNER_LOGIN, repository.owner.login);
        values.put(RepositoryEntry.COLUMN_NAME_REPOSITORY_AVATAR_URL, repository.owner.avatarUrl);
        return values;
    }

    public List<Repository> readRepositories() {
        SQLiteDatabase db = helper.getReadableDatabase();

        Cursor c = db.query(
                getTableName(),  // The table to query
                null,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        List<Repository> repositoryList = new LinkedList<>();

        while (c.moveToNext()) {
            Repository repository = new Repository();

            repository.id = c.getInt(0);
            repository.name = c.getString(1);
            repository.fullName = c.getString(2);
            repository.description = c.getString(3);
            repository.starsCount = c.getString(4);
            repository.watchersCount = c.getString(5);
            repository.defaultBranch = c.getString(6);

            Owner owner = new Owner();
            owner.login = c.getString(7);
            owner.avatarUrl = c.getString(8);

            repository.owner = owner;
            repositoryList.add(repository);
        }
        c.close();
        return repositoryList;
    }

    @Override
    public void removeData() {
        clearAllRepositories();
    }

    public long clearAllRepositories() {
        SQLiteDatabase db = helper.getWritableDatabase();
        return db.delete(getTableName(), null, null);
    }

}
