package bryn.com.githubclient.persistence.file;

import java.util.List;

import javax.inject.Inject;

import bryn.com.githubclient.persistence.DataRemover;
import rx.Observable;


public class RepoSearchWordsStorage implements DataRemover {

    private static final String TAG = "RepoSearchWordsStorage";
    private final StringsToFilePersistence filePersistence;

    @Inject
    public RepoSearchWordsStorage(StringsToFilePersistence filePersistence) {
        this.filePersistence = filePersistence;
        filePersistence.setFileName("SEARCH_WORDS.TXT");
    }

    public void addWord(String word) {
        filePersistence.addString(word);
    }


    public List<String> readWords() {
        return filePersistence.readStrings();
    }

    @Override
    public void removeData() {
        filePersistence.removeData();
    }

    public Observable<List<String>> readWordsAsync() {
        return filePersistence.readStringAsync();
    }

    public void addWordAsync(String word) {
        filePersistence.addStringAsync(word);
    }

}
