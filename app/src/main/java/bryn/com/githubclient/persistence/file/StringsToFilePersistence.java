package bryn.com.githubclient.persistence.file;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import bryn.com.githubclient.persistence.DataRemover;
import bryn.com.githubclient.utilities.Logger;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class StringsToFilePersistence implements DataRemover {

    private static final String TAG = "RepoSearchWordsStorage";
    private final Context context;
    private String fileName;

    @Inject
    public StringsToFilePersistence(Context context) {
        this.context = context;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean addString(String string) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(getFile(), true)))) {
            out.println(string);
        } catch (IOException e) {
            Logger.e("Exception", "File write failed: " + e.toString());
            return false;
        }
        return true;
    }

    public boolean addStrings(List<String> stringsTosave) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(getFile(), true)))) {
            for (int i = 0; i < stringsTosave.size(); i++) {
                out.println(stringsTosave.get(i));
            }
        } catch (IOException e) {
            Logger.e("Exception", "File write failed: " + e.toString());
            return false;
        }
        return true;
    }

    public List<String> readStrings() {
        List<String> searchStrings = new LinkedList<>();

        try (BufferedReader buf = new BufferedReader(new FileReader(getFile()))) {
            String line;
            while ((line = buf.readLine()) != null) {
                searchStrings.add(line);
            }
        } catch (IOException e) {
            Log.e("Exception", "File read failed: " + e.toString());
        }
        return searchStrings;
    }

    @Override
    public void removeData() {
        if (!getFile().delete()) {
            Logger.w(TAG, "Unable to delete strings file");
        }
    }

    public Observable<List<String>> readStringAsync() {
        return Observable.create(new Observable.OnSubscribe<List<String>>() {
            @Override
            public void call(Subscriber<? super List<String>> subscriber) {
                List<String> strings = readStrings();
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(strings);
                    subscriber.onCompleted();
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public void addStringAsync(String string) {
        Observable.just(string)
                .subscribeOn(Schedulers.io())
                .subscribe(saveWordAction);
    }

    private final Action1<String> saveWordAction = new Action1<String>() {
        @Override
        public void call(String s) {
            addString(s);
        }
    };

    @NonNull
    private File getFile() {
        return new File(context.getFilesDir(), fileName);
    }
}
