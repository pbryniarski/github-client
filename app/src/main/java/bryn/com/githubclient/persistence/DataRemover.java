package bryn.com.githubclient.persistence;

public interface DataRemover {
    void removeData();
}
