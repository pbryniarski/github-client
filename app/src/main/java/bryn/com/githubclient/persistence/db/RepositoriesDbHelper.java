package bryn.com.githubclient.persistence.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RepositoriesDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "repositories.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static final String SQL_DELETE_USER_ENTRIES =
            "DROP TABLE IF EXISTS " + RepositoriesStorage.RepositoryEntry.USER_REPOS_TABLE_NAME;

    private static final String SQL_DELETE_SEARCH_ENTRIES =
            "DROP TABLE IF EXISTS " + RepositoriesStorage.RepositoryEntry.SEARCH_REPOS_TABLE_NAME;

    @Inject
    public RepositoriesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(getCreateTableString(RepositoriesStorage.RepositoryEntry.USER_REPOS_TABLE_NAME));
        db.execSQL(getCreateTableString(RepositoriesStorage.RepositoryEntry.SEARCH_REPOS_TABLE_NAME));
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USER_ENTRIES);
        db.execSQL(SQL_DELETE_SEARCH_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private String getCreateTableString(String tableName){
        return "CREATE TABLE " + tableName + " (" +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_ID + " INTEGER PRIMARY KEY," +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_NAME + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_FULL_NAME + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_STAR_COUNT + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_WATCH_COUNT + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_DEFAULT_BRANCH + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_OWNER_LOGIN + TEXT_TYPE + COMMA_SEP +
                RepositoriesStorage.RepositoryEntry.COLUMN_NAME_REPOSITORY_AVATAR_URL + TEXT_TYPE +
                " )";
    }
}
