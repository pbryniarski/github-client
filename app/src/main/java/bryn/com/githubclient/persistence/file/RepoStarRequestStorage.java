package bryn.com.githubclient.persistence.file;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.interactor.starring.StarRequest;
import bryn.com.githubclient.persistence.DataRemover;

@Singleton
public class RepoStarRequestStorage implements DataRemover {

    private final StringsToFilePersistence filePersistence;
    private final List<StarRequest> requests = new LinkedList<>();

    @Inject
    public RepoStarRequestStorage(StringsToFilePersistence filePersistence) {
        this.filePersistence = filePersistence;
        filePersistence.setFileName("STAR_REQUESTS.TXT");
    }

    synchronized public void saveRequests() {
        List<String> serialized = new LinkedList<>();
        for (int i = 0; i < requests.size(); i++) {
            requests.get(i).serializeTo(serialized);
        }
        filePersistence.addStrings(serialized);
    }

    synchronized public void addRequest(StarRequest request) {
        int indexOfRequest = requests.indexOf(request);
        if (indexOfRequest == -1) {
            requests.add(request);
        } else {
            requests.set(indexOfRequest, request);
        }
    }

    synchronized public StarRequest getRequest() {
        if (requests.isEmpty()) {
            return null;
        }
        return requests.remove(0);
    }

    synchronized public void readRequests() {
        List<String> serialized = filePersistence.readStrings();
        for (int i = 0; i < serialized.size(); i = i + 3) {
            requests.add(new StarRequest(serialized.get(i), serialized.get(i + 1), Boolean.valueOf(serialized.get(i + 2))));
        }
        removeData();
    }

    @Override
    synchronized public void removeData() {
        filePersistence.removeData();
    }
}
