package bryn.com.githubclient.persistence.db;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pawelbryniarski on 17.12.15.
 */
@Singleton
public class UserRepositoriesStorage extends RepositoriesStorage {

    @Inject
    public UserRepositoriesStorage(RepositoriesDbHelper helper) {
        super(helper);
    }

    @Override
    protected String getTableName() {
        return RepositoryEntry.USER_REPOS_TABLE_NAME;
    }
}
