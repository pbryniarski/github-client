package bryn.com.githubclient.persistence;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import bryn.com.githubclient.persistence.db.SearchRepositoriesStorage;
import bryn.com.githubclient.persistence.db.UserRepositoriesStorage;
import bryn.com.githubclient.persistence.file.RepoSearchWordsStorage;
import bryn.com.githubclient.persistence.file.RepoStarRequestStorage;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;


public class OnLogoutDataRemover implements DataRemover {

    private final List<DataRemover> removers = new LinkedList<>();

    @Inject
    public OnLogoutDataRemover(SharedPreferencesUtil sharedPreferences,
                               RepoSearchWordsStorage searchWordsStorage,
                               RepoStarRequestStorage repoStarRequestStorage,
                               UserRepositoriesStorage userRepositoriesStorage,
                               SearchRepositoriesStorage searchRepositoriesStorage) {
        removers.add(sharedPreferences);
        removers.add(searchWordsStorage);
        removers.add(repoStarRequestStorage);
        removers.add(userRepositoriesStorage);
        removers.add(searchRepositoriesStorage);
    }

    @Override
    public void removeData() {
        for (int i = 0; i < removers.size(); i++) {
            removers.get(i).removeData();
        }
    }
}
