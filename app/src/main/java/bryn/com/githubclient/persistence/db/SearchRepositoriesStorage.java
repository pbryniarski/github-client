package bryn.com.githubclient.persistence.db;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SearchRepositoriesStorage extends RepositoriesStorage {

    @Inject
    public SearchRepositoriesStorage(RepositoriesDbHelper helper) {
        super(helper);
    }

    @Override
    protected String getTableName() {
        return RepositoryEntry.SEARCH_REPOS_TABLE_NAME;
    }
}
