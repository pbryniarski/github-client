package bryn.com.githubclient.dataModel;

/**
 * Created by pawelbryniarski on 13.12.15.
 */
public class RepoSearchResponse {
    public Repository[] items;
    public int maxPage;
}
