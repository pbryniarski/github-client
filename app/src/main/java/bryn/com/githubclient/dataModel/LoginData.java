package bryn.com.githubclient.dataModel;

public class LoginData {

    public final String login;
    public final String password;
    public final String tfaCode;

    public LoginData(String login, String password, String tfaCode) {
        this.login = login;
        this.password = password;
        this.tfaCode = tfaCode;
    }

    public LoginData(String login, String password) {
        this.login = login;
        this.password = password;
        this.tfaCode = null;
    }

    //auto generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginData loginData = (LoginData) o;

        if (login != null ? !login.equals(loginData.login) : loginData.login != null) return false;
        if (password != null ? !password.equals(loginData.password) : loginData.password != null)
            return false;
        return !(tfaCode != null ? !tfaCode.equals(loginData.tfaCode) : loginData.tfaCode != null);

    }

    //auto generated
    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (tfaCode != null ? tfaCode.hashCode() : 0);
        return result;
    }
}
