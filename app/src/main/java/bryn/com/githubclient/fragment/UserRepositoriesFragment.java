package bryn.com.githubclient.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.adapter.RepositoriesAdapter;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.presenter.UserRepositoriesPresenter;
import bryn.com.githubclient.utilities.ResourcesProvider;
import bryn.com.githubclient.utilities.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.subscriptions.CompositeSubscription;


public class UserRepositoriesFragment extends Fragment {

    private static final String TAG = "RepositoriesFragment";

    @Bind(R.id.repositories_recycler_view)
    RecyclerView repositoriesRecycler;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @Bind(R.id.fragment_repositories)
    View fragmentRepositories;

    @Inject
    UserRepositoriesPresenter userRepositoriesPresenter;

    @Inject
    ResourcesProvider resourcesProvider;

    @Inject
    Utils utils;

    @Inject
    ActionBar actionBar;

    @Inject
    Picasso picasso;

    private CompositeSubscription subscription;
    private RepositoriesAdapter repositoriesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repositories, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectDependencies();
        setUpTitleAndNavigation();
        subscription = new CompositeSubscription();

        userRepositoriesPresenter.setFragment(this);
        setUpRecyclerView();
        refreshLayout.setColorSchemeColors(R.color.colorPrimary);
        setRefreshListener();
        subscription.add(
                userRepositoriesPresenter.getRepos());
    }

    public void onNewRepositories(List<Repository> repositories) {
        repositoriesAdapter.setRepositories(repositories);
    }

    public void hideProgress() {
        setRefreshing(false);
    }

    public void showProgress() {
        setRefreshing(true);
    }

    public void showRepoFetchError() {
        utils.showSnackBar(fragmentRepositories, resourcesProvider.getString(R.string.error_fetch_repos));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.user_repo_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                userRepositoriesPresenter.onSearchRequested();
                return true;
            case R.id.action_logout:
                userRepositoriesPresenter.onLogout();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        subscription.unsubscribe();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            actionBar.setTitle(resourcesProvider.getString(R.string.repos_title));
        }
    }

    private void setRefreshing(final boolean refreshing) {
        //due to bug in refresh layout
        refreshLayout.getViewTreeObserver()
                .addOnGlobalLayoutListener(
                        new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                refreshLayout
                                        .getViewTreeObserver()
                                        .removeOnGlobalLayoutListener(this);
                                refreshLayout.setRefreshing(refreshing);
                            }
                        });
    }

    private void setUpRecyclerView() {
        repositoriesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        repositoriesAdapter = new RepositoriesAdapter(getActivity().getLayoutInflater(), picasso, userRepositoriesPresenter);
        repositoriesRecycler.setAdapter(repositoriesAdapter);
    }

    private void injectDependencies() {
        ((MainActivity) getActivity()).getActivityComponent().inject(this);
    }

    private void setUpTitleAndNavigation() {
        actionBar.setTitle(resourcesProvider.getString(R.string.repos_title));
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    private void setRefreshListener() {
        SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                subscription.add(
                        userRepositoriesPresenter.onRefreshSwiped()
                );
            }
        };
        refreshLayout.setOnRefreshListener(refreshListener);
    }

}
