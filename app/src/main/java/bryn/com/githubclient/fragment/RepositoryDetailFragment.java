package bryn.com.githubclient.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.presenter.RepoDetailPresenter;
import bryn.com.githubclient.utilities.ResourcesProvider;
import bryn.com.githubclient.utilities.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.subscriptions.CompositeSubscription;

public class RepositoryDetailFragment extends Fragment {

    private static final String REPOSITORY_KEY = "REPOSITORY";

    @Bind(R.id.detail_view)
    View detailRepoView;

    @Bind(R.id.image_avatar)
    ImageView avatar;

    @Bind(R.id.full_name)
    TextView fullNameTextView;

    @Bind(R.id.default_branch)
    TextView defaultBranchTextView;

    @Bind(R.id.star_count)
    TextView starCountTextView;

    @Bind(R.id.watch_count)
    TextView watchCountTextView;

    @Bind(R.id.owner)
    TextView ownerTextView;

    @Bind(R.id.description)
    TextView descriptionTextView;

    @Bind(R.id.start_unstar)
    CheckBox repoStarredCheckbox;

    @Inject
    RepoDetailPresenter presenter;

    @Inject
    Utils utils;

    @Inject
    ResourcesProvider resources;

    @Inject
    ActionBar actionBar;

    @Inject
    Picasso picasso;

    private CompositeSubscription subscription;

    public static RepositoryDetailFragment getFragment(Repository repository) {
        RepositoryDetailFragment fragment = new RepositoryDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(REPOSITORY_KEY, repository);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repository_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        subscription = new CompositeSubscription();
        final Repository repository = getArguments().getParcelable(REPOSITORY_KEY);
        injectDependencies();
        setTitleAndUpNavigation(repository);
        setUpStarCheckBox(repository);
        bindDataToView(repository);
        presenter.setFragment(this);
        subscription.add(presenter.getStarState(repository));
    }

    public void setStarSelected() {
        repoStarredCheckbox.setChecked(true);
    }

    public void setStarUnselected() {
        repoStarredCheckbox.setChecked(false);
    }

    public void showNetworkErrorInfo() {
        utils.showSnackBar(detailRepoView, resources.getString(R.string.network_error_data_stale));
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            actionBar.setTitle("");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        actionBar.setDisplayHomeAsUpEnabled(false);
        subscription.unsubscribe();
    }

    private void setUpStarCheckBox(final Repository repository) {
        repoStarredCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean star = ((CheckBox) v).isChecked();
                presenter.starRepo(repository, star);
            }
        });
    }

    private void setTitleAndUpNavigation(Repository repository) {
        actionBar.setTitle(repository.name);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void injectDependencies() {
        ((MainActivity) getActivity()).getActivityComponent().inject(this);
    }

    private void bindDataToView(Repository repository) {
        picasso.load(repository.owner.avatarUrl).into(avatar);
        fullNameTextView.setText(repository.fullName);
        defaultBranchTextView.setText(repository.defaultBranch);
        starCountTextView.setText(repository.starsCount);
        watchCountTextView.setText(repository.watchersCount);
        ownerTextView.setText(repository.owner.login);
        descriptionTextView.setText(repository.description);
    }
}
