package bryn.com.githubclient.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.adapter.RepositoriesAdapter;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.presenter.RepoSearchPresenter;
import bryn.com.githubclient.utilities.Logger;
import bryn.com.githubclient.utilities.ResourcesProvider;
import bryn.com.githubclient.utilities.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SearchFragment extends Fragment {

    private static final String TAG = "SearchFragment";
    private static final String REPOSITORIES = "REPOSITORIES";

    @Bind(R.id.repositories_recycler_view)
    RecyclerView repositoriesRecycler;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @Bind(R.id.fragment_repositories)
    View fragmentRepositories;

    @Bind(R.id.more_repos_progress)
    ProgressBar progressBar;

    @Inject
    ActionBar actionBar;

    @Inject
    ImageButton clearTextButton;

    @Inject
    @Named("search_bar")
    View searchBar;

    @Inject
    AutoCompleteTextView searchText;

    @Inject
    Toolbar toolbar;

    @Inject
    RepoSearchPresenter presenter;

    @Inject
    Picasso picasso;

    @Inject
    Utils utils;

    @Inject
    ResourcesProvider resourcesProvider;

    private RepositoriesAdapter repositoriesAdapter;
    private ArrayAdapter<String> autocompleteSearchAdapter;
    private CompositeSubscription rxSubscriptions;

    //For detecting recycler view hit bottom
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repositories, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        refreshLayout.setEnabled(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectDependencies();
        setTitleAndUpNavigation();

        rxSubscriptions = new CompositeSubscription();
        setUpRecyclerView();
        presenter.setFragment(this);
        restoreState(savedInstanceState);
        searchBar.setVisibility(View.VISIBLE);
        setUpSearchView();
        setUpClearButton();
        rxSubscriptions.add(
                presenter.getSearchHistory()
        );

        if (savedInstanceState == null) {
            addSubscriptionIfNotNull(presenter.restoreLastSearch());
        }
    }

    public void setSearchWord(String searchWord) {
        searchText.setText(searchWord);
    }

    public void hideProgress() {
        loading = false;
        progressBar.setVisibility(View.GONE);
    }

    public void showRepoFetchError() {
        utils.showSnackBar(fragmentRepositories, resourcesProvider.getString(R.string.error_fetch_repos));
    }

    public void onNewRepositories(List<Repository> repositories) {
        repositoriesAdapter.addRepositories(repositories);
    }


    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void clearRepos() {
        repositoriesAdapter.clearRepos();
    }

    public void setSearchHistory(List<String> searchHistory) {
        autocompleteSearchAdapter = new ArrayAdapter<>(getActivity(), R.layout.search_autocomplete_item, searchHistory);
        searchText.setAdapter(autocompleteSearchAdapter);
    }

    public void refreshSearchHistory(Collection<String> words) {
        if (autocompleteSearchAdapter != null) {
            autocompleteSearchAdapter.clear();
            autocompleteSearchAdapter.addAll(words);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<Repository> reposToSave = new ArrayList<>();
        reposToSave.addAll(repositoriesAdapter.getRepositories());
        outState.putParcelableArrayList(REPOSITORIES, reposToSave);

        presenter.saveState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rxSubscriptions.unsubscribe();
        toolbar.setBackgroundResource(R.color.colorPrimary);
        hideSearchBarAndUpArrow();
        ButterKnife.unbind(this);
    }

    private void hideSearchBarAndUpArrow() {
        actionBar.setDisplayHomeAsUpEnabled(false);
        searchBar.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.saveLastSearch(repositoriesAdapter.getRepositories());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            hideSearchBarAndUpArrow();
        } else {
            actionBar.setDisplayHomeAsUpEnabled(true);
            searchBar.setVisibility(View.VISIBLE);
        }
    }

    private void setUpClearButton() {

        clearTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setText("");
            }
        });
    }

    private void setUpSearchView() {
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    addSubscriptionIfNotNull(presenter.getFirstPage(searchText.getText().toString()));
                    searchText.dismissDropDown();
                    hideKeyboard(v);
                    return true;
                }
                return false;
            }
        });
        searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.showDropDown();
            }
        });

        searchText.setThreshold(0);
    }

    private void addSubscriptionIfNotNull(Subscription subscription) {
        if (subscription != null) {
            rxSubscriptions.add(subscription);
        }
    }

    private void hideKeyboard(TextView v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            ArrayList<Repository> r = savedInstanceState.getParcelableArrayList(REPOSITORIES);
            repositoriesAdapter.setRepositories(r);
            presenter.restoreState(savedInstanceState);
        }
    }

    private void setUpRecyclerView() {
        repositoriesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        repositoriesAdapter = new RepositoriesAdapter(getActivity().getLayoutInflater(), picasso, presenter);
        repositoriesRecycler.setAdapter(repositoriesAdapter);

        repositoriesRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = repositoriesRecycler.getChildCount();
                int totalItemCount = repositoriesRecycler.getLayoutManager().getItemCount();
                int firstVisibleItem = ((LinearLayoutManager) repositoriesRecycler.getLayoutManager()).findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    loading = true;
                    Logger.d(TAG, " Recycler hit bottom, need more repos");
                    presenter.getNextPage();

                }
            }
        });
    }

    private void setTitleAndUpNavigation() {
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void injectDependencies() {
        ((MainActivity) getActivity()).getActivityComponent().inject(this);
    }
}
