package bryn.com.githubclient.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import javax.inject.Inject;

import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.presenter.LoginPresenter;
import bryn.com.githubclient.utilities.ResourcesProvider;
import bryn.com.githubclient.utilities.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class LoginFragment extends Fragment {

    @Bind(R.id.login)
    EditText loginEditText;

    @Bind(R.id.password)
    EditText passwordEditText;

    @Bind(R.id.tfa_code)
    EditText tfaEditText;

    @Bind(R.id.tfa_code_hint)
    View tfaHint;

    @Bind(R.id.login_button)
    View loginButton;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.login_fragment)
    View loginFragmentLayout;

    @Inject
    LoginPresenter presenter;

    @Inject
    Utils utils;

    @Inject
    ResourcesProvider resourcesProvider;

    private CompositeSubscription subscriptions;

    @OnClick(R.id.login_button)
    public void login() {
        String login = loginEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String tfa = tfaEditText.getText().toString();

        if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
            if (!TextUtils.isEmpty(tfa)) {
                subscriptions.add(presenter.login(login, password, tfa));
            } else {
                subscriptions.add(presenter.login(login, password));
            }
        } else {
            utils.showSnackBar(loginFragmentLayout, resourcesProvider.getString(R.string.error_login_fields));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        injectDependencies();
        presenter.setFragment(this);
        subscriptions = new CompositeSubscription();
        WorkerFragment fragment = getWorkerFragment();

        if (fragment != null) {
            addSubscriptionIfNotNull(presenter.restoreLoggingState(fragment));
        }
    }

    public void onLoginFailure() {
        utils.showSnackBar(loginFragmentLayout, resourcesProvider.getString(R.string.login_failed));
    }

    public void onTfaFailure() {
        tfaHint.setVisibility(View.VISIBLE);
        utils.showSnackBar(loginFragmentLayout, resourcesProvider.getString(R.string.tfa_required));
    }

    public void onLoginStart() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void onLoginStop() {
        progressBar.setVisibility(View.GONE);
        tfaHint.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null) {
            presenter.saveLoggingState(getWorkerFragment());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        subscriptions.unsubscribe();
    }

    private void injectDependencies() {
        ((MainActivity) getActivity()).getActivityComponent().inject(this);
    }

    private void addSubscriptionIfNotNull(Subscription subscription) {
        if (subscription == null) {
            return;
        }
        subscriptions.add(subscription);
    }

    private WorkerFragment getWorkerFragment() {
        return (WorkerFragment) getActivity().getFragmentManager().findFragmentByTag(MainActivity.WORKER_FRAGMENT);
    }
}
