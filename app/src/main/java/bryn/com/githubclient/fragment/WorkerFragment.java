package bryn.com.githubclient.fragment;

import android.app.Fragment;
import android.os.Bundle;

import rx.Observable;

public class WorkerFragment extends Fragment {

    public Observable<Void> loginObservable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
