package bryn.com.githubclient.interactor.starring;

import java.util.List;

public class StarRequest {

    public String repositoryName;
    public String owner;
    public boolean shouldBeStarred;

    public StarRequest(String repositoryName, String owner, boolean shouldBeStarred) {
        this.repositoryName = repositoryName;
        this.owner = owner;
        this.shouldBeStarred = shouldBeStarred;
    }

    public void serializeTo(List<String> serialized) {
        serialized.add(repositoryName);
        serialized.add(owner);
        serialized.add(String.valueOf(shouldBeStarred));
    }

    //shouldBeStarred ignored
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StarRequest request = (StarRequest) o;

        if (repositoryName != null ? !repositoryName.equals(request.repositoryName) : request.repositoryName != null)
            return false;
        return !(owner != null ? !owner.equals(request.owner) : request.owner != null);

    }

    @Override
    public int hashCode() {
        int result = repositoryName != null ? repositoryName.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StarRequest{" +
                "repositoryName='" + repositoryName + '\'' +
                ", owner='" + owner + '\'' +
                ", shouldBeStarred=" + shouldBeStarred +
                '}';
    }
}
