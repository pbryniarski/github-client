package bryn.com.githubclient.interactor.repositories;

import java.util.List;

import bryn.com.githubclient.dataModel.Repository;

public class UserRepoRequest {
    public String credentials;
    public List<Repository> repositoryList;
    public int maxPages;
    public int pageNumber;
}
