package bryn.com.githubclient.interactor.starring;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.persistence.file.RepoStarRequestStorage;
import bryn.com.githubclient.utilities.CredentialsProvider;
import retrofit.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

@Singleton
public class RepoStarredInteractor {

    private static final int NO_CONTENT_OK = 204;
    private static final String TAG = "RepoStarInteractor";

    private final GithubApi api;
    private final CredentialsProvider credentialsProvider;
    private final RepoStarRequestStorage requestStorage;

    @Inject
    public RepoStarredInteractor(GithubApi api,
                                 CredentialsProvider credentialsProvider,
                                 RepoStarRequestStorage requestStorage) {
        this.api = api;
        this.credentialsProvider = credentialsProvider;
        this.requestStorage = requestStorage;
    }

    public Observable<Boolean> isRepoStarred(Repository repository) {

        return api.isStarred(credentialsProvider.getBasicAuth(), repository.owner.login, repository.name)
                .map(new Func1<Response<Void>, Boolean>() {
                    @Override
                    public Boolean call(Response<Void> response) {
                        return response.code() == NO_CONTENT_OK;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Void> setRepoStarred(final boolean star, final Repository repository) {
        return getStarObservable(new StarRequest(repository.name, repository.owner.login, star));
    }

    private Observable<Void> getStarObservable(final StarRequest request) {
        Observable<Response<Void>> observable;
        if (request.shouldBeStarred) {
            observable = api.starRepo(credentialsProvider.getBasicAuth(), request.owner, request.repositoryName);
        } else {
            observable = api.unstarRepo(credentialsProvider.getBasicAuth(), request.owner, request.repositoryName);
        }
        return observable
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof IOException) {
                            requestStorage.addRequest(request);
                        }
                    }
                }).map(new Func1<Response<Void>, Void>() {
                    @Override
                    public Void call(Response<Void> voidResponse) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
