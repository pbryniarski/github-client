package bryn.com.githubclient.interactor.repositories;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.persistence.db.UserRepositoriesStorage;
import bryn.com.githubclient.utilities.CredentialsProvider;
import bryn.com.githubclient.utilities.Logger;
import retrofit.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by pawelbryniarski on 09.12.15.
 */
@Singleton
public class UserRepositoriesInteractor {

    private static final String TAG = "UserReposInteractor";
    private final GithubApi api;
    private final CredentialsProvider credentialsProvider;
    private final PageNumberParser pageNumberParser;
    private final UserRepositoriesStorage storage;

    private PublishSubject<List<Repository>> reposFetchInProgress;

    @Inject
    public UserRepositoriesInteractor(GithubApi api,
                                      CredentialsProvider credentialsProvider,
                                      PageNumberParser pageNumberParser,
                                      UserRepositoriesStorage storage) {
        this.api = api;
        this.credentialsProvider = credentialsProvider;
        this.pageNumberParser = pageNumberParser;
        this.storage = storage;
    }

    public Observable<List<Repository>> getReposFetchInProgress() {
        if (reposFetchInProgress == null) {
            return null;
        }
        return reposFetchInProgress;
    }

    public Observable<List<Repository>> getRepositories() {
        reposFetchInProgress = PublishSubject.create();

        reposFromStorageObservable
                .flatMap(ifEmptyDownloadRepos)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNewRepos, onNewReposError, onNewReposCompleted);
        return reposFetchInProgress;
    }

    private final Func1<List<Repository>, Observable<List<Repository>>> ifEmptyDownloadRepos =
            new Func1<List<Repository>, Observable<List<Repository>>>() {
                @Override
                public Observable<List<Repository>> call(List<Repository> repositories) {
                    if (repositories.isEmpty()) {
                        return getDownloadReposObservable();
                    }
                    return Observable.just(repositories);
                }
            };

    public Observable<List<Repository>> downloadRepositories() {
        reposFetchInProgress = PublishSubject.create();
        getDownloadReposObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNewRepos, onNewReposError, onNewReposCompleted);
        return reposFetchInProgress;
    }

    /* Download and save all user repositories */
    @NonNull
    private Observable<List<Repository>> getDownloadReposObservable() {
        Logger.d(TAG, "getDownloadReposObservable");
        return Observable.just(getFirstPageRequest())
                .doOnNext(getReposFromFirstPage)
                .doOnNext(saveRepositories)
                .doOnNext(pushOnNextOut)
                .flatMap(createRequestsForNextPages)
                .scan(downloadSaveAndPushOutNextPage)
                .doOnNext(clearRepositories)
                .doOnNext(saveRepositories)
                .map(getReposFromRequest);
    }

    @NonNull
    private UserRepoRequest getFirstPageRequest() {
        final UserRepoRequest repoRequest = new UserRepoRequest();
        repoRequest.credentials = credentialsProvider.getBasicAuth();
        repoRequest.pageNumber = 1;
        repoRequest.repositoryList = new LinkedList<>();
        return repoRequest;
    }

    private Action1<UserRepoRequest> getReposFromFirstPage =
            new Action1<UserRepoRequest>() {
                @Override
                public void call(UserRepoRequest userRepoRequest) {
                    Response<Repository[]> response = downloadRepositories(userRepoRequest);
                    userRepoRequest.repositoryList.addAll(Arrays.asList(response.body()));
                    userRepoRequest.maxPages = pageNumberParser.getLastPageNumber(response.raw().header("Link"));
                }
            };

    private Response<Repository[]> downloadRepositories(UserRepoRequest repoRequest) {
        Response<Repository[]> response;
        try {
            response = api.getUserReposSynchronous(repoRequest.credentials, repoRequest.pageNumber).execute();
        } catch (IOException e) {
            throw Exceptions.propagate(e);
        }
        return response;
    }

    private final Action1<UserRepoRequest> saveRepositories = new Action1<UserRepoRequest>() {
        @Override
        public void call(UserRepoRequest userRepoRequest) {
            storage.saveRepositories(userRepoRequest.repositoryList);
        }
    };

    private Action1<UserRepoRequest> pushOnNextOut = new Action1<UserRepoRequest>() {
        @Override
        public void call(UserRepoRequest userRepoRequest) {
            reposFetchInProgress.onNext(userRepoRequest.repositoryList);
        }
    };

    private final Func1<UserRepoRequest, Observable<UserRepoRequest>> createRequestsForNextPages =
            new Func1<UserRepoRequest, Observable<UserRepoRequest>>() {
                @Override
                public Observable<UserRepoRequest> call(UserRepoRequest userRepoRequest) {
                    UserRepoRequest[] requests = new UserRepoRequest[userRepoRequest.maxPages];
                    for (int i = 1; i < requests.length; i++) {
                        requests[i] = new UserRepoRequest();
                        requests[i].pageNumber = userRepoRequest.pageNumber + i;
                        requests[i].credentials = userRepoRequest.credentials;
                    }
                    requests[0] = userRepoRequest;
                    return Observable.from(requests);
                }
            };

    private final Func2<UserRepoRequest, UserRepoRequest, UserRepoRequest> downloadSaveAndPushOutNextPage =
            new Func2<UserRepoRequest, UserRepoRequest, UserRepoRequest>() {
                @Override
                public UserRepoRequest call(UserRepoRequest sum, UserRepoRequest incoming) {
                    Response<Repository[]> response = downloadRepositories(incoming);
                    List<Repository> newRepos = Arrays.asList(response.body());
                    storage.saveRepositories(newRepos);
                    sum.repositoryList.addAll(newRepos);
                    reposFetchInProgress.onNext(sum.repositoryList);
                    return sum;
                }
            };

    private Action1<UserRepoRequest> clearRepositories = new Action1<UserRepoRequest>() {
        @Override
        public void call(UserRepoRequest userRepoRequest) {
            storage.clearAllRepositories();
        }
    };

    private final Func1<UserRepoRequest, List<Repository>> getReposFromRequest =
            new Func1<UserRepoRequest, List<Repository>>() {
                @Override
                public List<Repository> call(UserRepoRequest userRepoRequest) {
                    return userRepoRequest.repositoryList;
                }
            };
    /* Download and save all user repositories */

    private final Observable<List<Repository>> reposFromStorageObservable =
            Observable.create(new Observable.OnSubscribe<List<Repository>>() {
                @Override
                public void call(Subscriber<? super List<Repository>> subscriber) {
                    if (!subscriber.isUnsubscribed()) {
                        Logger.d(TAG, "getting repos from storage");
                        subscriber.onNext(storage.readRepositories());
                        subscriber.onCompleted();
                    }
                }
            });

    private Action1<List<Repository>> onNewRepos = new Action1<List<Repository>>() {
        @Override
        public void call(List<Repository> repositories) {
            reposFetchInProgress.onNext(repositories);
        }
    };

    private Action1<Throwable> onNewReposError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            reposFetchInProgress.onError(throwable);
            reposFetchInProgress = null;
        }
    };

    private Action0 onNewReposCompleted = new Action0() {
        @Override
        public void call() {
            reposFetchInProgress.onCompleted();
            reposFetchInProgress = null;
        }
    };

}
