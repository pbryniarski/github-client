package bryn.com.githubclient.interactor.repositories;


import android.support.annotation.Nullable;

import javax.inject.Inject;

public class PageNumberParser {

    @Inject
    public PageNumberParser() {
    }

    public int getLastPageNumber(String linkHeader) {
        return getPageNumber("\"last\"", linkHeader);
    }

    // Example string to parse
    // "<https://api.github.com/user/82592/repos?page=2>; rel=\"next\", <https://api.github.com/user/82592/repos?page=6>; rel=\"last\"";

    private int getPageNumber(String linkType, String linkHeader) {
        if (isEmpty(linkHeader)) {
            return -1;
        }
        String[] pages = linkHeader.split(",");
        String desiredPageLink = findStringThatContains(linkType, pages);

        if (isEmpty(desiredPageLink)) {
            return -1;
        }

        desiredPageLink = desiredPageLink.split(";")[0].replace(">", "");

        int queryPartStart = desiredPageLink.indexOf("?");

        String wholeQuery = desiredPageLink.substring(queryPartStart);

        String queries[] = wholeQuery.split("&");

        String pageQuery = findStringThatContains("page=", queries);

        String nextPageNumber = pageQuery.split("=")[1];
        return Integer.valueOf(nextPageNumber);
    }

    private String findStringThatContains(String s, String[] pages) {
        for (String page : pages) {
            if (page.contains(s)) {
                return page;
            }
        }
        return null;
    }

    private boolean isEmpty(@Nullable String str) {
        return str == null || str.length() == 0;
    }
}
