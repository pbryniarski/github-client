package bryn.com.githubclient.interactor.starring;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import javax.inject.Inject;

import bryn.com.githubclient.App;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.persistence.file.RepoStarRequestStorage;
import bryn.com.githubclient.utilities.CredentialsProvider;
import bryn.com.githubclient.utilities.Logger;
import retrofit.Call;
import retrofit.Response;

public class StarRequestSyncService extends IntentService {

    @Inject
    RepoStarredInteractor repoStarredInteractor;

    @Inject
    RepoStarRequestStorage requestStorage;

    @Inject
    GithubApi api;

    @Inject
    CredentialsProvider credentialsProvider;

    public StarRequestSyncService() {
        super("MyIntentService");
    }

    public static void executeRequests(Context context) {
        Intent intent = new Intent(context, StarRequestSyncService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((App) getApplication()).getAppComponent().inject(this);

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        requestStorage.readRequests();
        StarRequest request;
        while ((request = requestStorage.getRequest()) != null) {
            Call<ResponseBody> call;
            if (request.shouldBeStarred) {
                call = api.starRepoSync(credentialsProvider.getBasicAuth(), request.owner, request.repositoryName);
            } else {
                call = api.unstarRepoSync(credentialsProvider.getBasicAuth(), request.owner, request.repositoryName);
            }

            try {
                //I assume call was successful
                Response<ResponseBody> response = call.execute();
                Logger.d("Star sync", "Star sync request " + request + " with code: " + response.code());
            } catch (IOException e) {
                // No network, wont sync, we have to put back the request
                requestStorage.addRequest(request);
                break;
            }

        }


    }

}
