package bryn.com.githubclient.interactor.repositories;

import javax.inject.Inject;

import bryn.com.githubclient.dataModel.RepoSearchResponse;
import bryn.com.githubclient.network.GithubApi;
import retrofit.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 13.12.15.
 */
public class SearchReposInteractor {

    private static final String TAG = "SearchReposInteractor";
    private final GithubApi api;
    private final PageNumberParser pageNumberParser;

    @Inject
    public SearchReposInteractor(GithubApi api, PageNumberParser pageNumberParser) {
        this.api = api;
        this.pageNumberParser = pageNumberParser;
    }

    public Observable<RepoSearchResponse> search(String keyword, int page) {
        return api.search(keyword, page)
                .doOnNext(new Action1<Response<RepoSearchResponse>>() {
                    @Override
                    public void call(Response<RepoSearchResponse> repoSearchResponseResponse) {
                        repoSearchResponseResponse.body().maxPage =
                                pageNumberParser.getLastPageNumber(repoSearchResponseResponse.headers().get("Link"));
                    }
                }).map(new Func1<Response<RepoSearchResponse>, RepoSearchResponse>() {
                    @Override
                    public RepoSearchResponse call(Response<RepoSearchResponse> repoSearchResponseResponse) {
                        return repoSearchResponseResponse.body();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
