package bryn.com.githubclient;

import android.app.FragmentManager;

import javax.inject.Inject;

import bryn.com.githubclient.dagger.MainActivityScope;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.fragment.LoginFragment;
import bryn.com.githubclient.fragment.RepositoryDetailFragment;
import bryn.com.githubclient.fragment.SearchFragment;
import bryn.com.githubclient.fragment.UserRepositoriesFragment;
import bryn.com.githubclient.persistence.OnLogoutDataRemover;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;

@MainActivityScope
public class NavigationController {

    private final FragmentManager fragmentManager;
    private final SharedPreferencesUtil sharedPreferences;
    private final OnLogoutDataRemover remover;

    @Inject
    public NavigationController(FragmentManager fragmentManager,
                                SharedPreferencesUtil sharedPreferences,
                                OnLogoutDataRemover remover) {
        this.fragmentManager = fragmentManager;
        this.sharedPreferences = sharedPreferences;
        this.remover = remover;
    }

    public void userLoggedIn() {
        UserRepositoriesFragment fragment = new UserRepositoriesFragment();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack("")
                .commit();
    }

    public void showInitialScreen() {
        if (sharedPreferences.getCredentials() != null) {
            userLoggedIn();
        } else {
            LoginFragment loginFragment = new LoginFragment();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, loginFragment)
                    .commit();
        }
    }

    public void showRepoDetails(Repository repository) {
        RepositoryDetailFragment repositoryDetailFragment = RepositoryDetailFragment.getFragment(repository);
        fragmentManager
                .beginTransaction()
                .hide(fragmentManager.findFragmentById(R.id.fragment_container))
                .add(R.id.fragment_container, repositoryDetailFragment)
                .addToBackStack("")
                .commit();
    }

    public boolean onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
            return true;
        }
        return false;
    }


    public void showSearch() {
        SearchFragment fragment = new SearchFragment();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack("show")
                .commit();
    }

    public void logout() {
        remover.removeData();
        LoginFragment fragment = new LoginFragment();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}
