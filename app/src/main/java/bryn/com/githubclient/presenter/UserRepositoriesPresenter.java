package bryn.com.githubclient.presenter;

import java.util.List;

import javax.inject.Inject;

import bryn.com.githubclient.NavigationController;
import bryn.com.githubclient.adapter.RepositoriesAdapter;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.fragment.UserRepositoriesFragment;
import bryn.com.githubclient.interactor.repositories.UserRepositoriesInteractor;
import bryn.com.githubclient.utilities.Logger;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;

public class UserRepositoriesPresenter implements RepositoriesAdapter.OnRepoClickedListener {

    private static final String TAG = "RepositoriesPresenter";
    private final UserRepositoriesInteractor userRepositoriesInteractor;
    private final NavigationController navigationController;
    private UserRepositoriesFragment fragment;

    @Inject
    public UserRepositoriesPresenter(UserRepositoriesInteractor userRepositoriesInteractor, NavigationController navigationController) {
        this.userRepositoriesInteractor = userRepositoriesInteractor;
        this.navigationController = navigationController;
    }

    public void setFragment(UserRepositoriesFragment fragment) {
        this.fragment = fragment;
    }

    public Subscription onRefreshSwiped() {
        fragment.showProgress();
        Logger.d(TAG, "onRefreshSwiped");
        return userRepositoriesInteractor
                .downloadRepositories()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        onNextRepos,
                        onReposFetchError,
                        onReposFetchCompleted);
    }

    public Subscription getRepos() {
        fragment.showProgress();
        Observable<List<Repository>> repoObservable;

        if (userRepositoriesInteractor.getReposFetchInProgress() != null) {
            Logger.d(TAG, "getRepos Already getting repos");
            repoObservable = userRepositoriesInteractor.getReposFetchInProgress();
        } else {
            Logger.d(TAG, "getRepos Not getting repos, will try to get them");
            repoObservable = userRepositoriesInteractor.getRepositories();
        }

        return repoObservable.observeOn(AndroidSchedulers.mainThread()).subscribe(
                onNextRepos,
                onReposFetchError,
                onReposFetchCompleted);
    }

    private final Action0 onReposFetchCompleted = new Action0() {
        @Override
        public void call() {
            Logger.d(TAG, "Getting repos completed");
            fragment.hideProgress();
        }
    };

    private final Action1<Throwable> onReposFetchError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            Logger.e(TAG, "Getting repos failed:", throwable);
            fragment.hideProgress();
            fragment.showRepoFetchError();
        }
    };

    private final Action1<List<Repository>> onNextRepos = new Action1<List<Repository>>() {
        @Override
        public void call(List<Repository> repositories) {
            fragment.onNewRepositories(repositories);
            Logger.d(TAG, "New repos delivered");
        }
    };

    @Override
    public void onClicked(Repository repository) {
        navigationController.showRepoDetails(repository);
    }


    public void onSearchRequested() {
        navigationController.showSearch();
    }

    public void onLogout() {
        navigationController.logout();
    }
}

