package bryn.com.githubclient.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.squareup.okhttp.Headers;

import javax.inject.Inject;

import bryn.com.githubclient.NavigationController;
import bryn.com.githubclient.dataModel.LoginData;
import bryn.com.githubclient.fragment.LoginFragment;
import bryn.com.githubclient.fragment.WorkerFragment;
import bryn.com.githubclient.interactor.LoginInteractor;
import bryn.com.githubclient.utilities.Logger;
import retrofit.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public class LoginPresenter {

    private static final String TAG = "LoginPresenter";

    private final LoginInteractor loginInteractor;
    private final NavigationController navigationController;

    private LoginFragment fragment;

    @VisibleForTesting
    Observable<Void> loginObservable;

    @Inject
    public LoginPresenter(LoginInteractor loginInteractor, NavigationController navigationController) {
        this.loginInteractor = loginInteractor;
        this.navigationController = navigationController;
    }

    public Subscription login(String login, String password) {
        Logger.v(TAG, "login: " + login + " password " + password);
        fragment.onLoginStart();
        loginObservable = loginInteractor.login(new LoginData(login, password));
        return loginObservable
                .subscribe(loginSuccess, loginError);
    }

    public Subscription login(String login, String password, String tfaCode) {
        Logger.v(TAG, "login: " + login + " password " + password + "tfaCode: " + tfaCode);
        fragment.onLoginStart();
        loginObservable = loginInteractor.login(new LoginData(login, password, tfaCode));
        return loginObservable.subscribe(loginSuccess, loginError);
    }

    public void setFragment(LoginFragment fragment) {
        this.fragment = fragment;
    }

    public Subscription restoreLoggingState(@NonNull WorkerFragment workerFragment) {
        if (workerFragment.loginObservable != null) {
            Logger.v(TAG, "restoreLoggingState will continue log in");
            fragment.onLoginStart();
            loginObservable = workerFragment.loginObservable;
            Subscription subscription = workerFragment.loginObservable.subscribe(loginSuccess, loginError);
            workerFragment.loginObservable = null;
            return subscription;
        }
        return null;
    }

    public void saveLoggingState(WorkerFragment workerFragment) {
        if (loginObservable != null) {
            Logger.v(TAG, "saveLoggingState loginObservable");
            workerFragment.loginObservable = loginObservable;
        }
    }

    @VisibleForTesting
    final Action1<Void> loginSuccess = new Action1<Void>() {
        @Override
        public void call(Void aVoid) {
            Logger.v(TAG, "login success");
            fragment.onLoginStop();
            navigationController.userLoggedIn();
            loginObservable = null;
        }
    };

    @VisibleForTesting
    final Action1<Throwable> loginError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            fragment.onLoginStop();
            Logger.w(TAG, "loginError login not successful");
            if (retrofitError(throwable)) {
                HttpException exception = (HttpException) throwable;
                String tfaHeader = getTfaHeader(exception);
                if (tfaRequired(tfaHeader) && errorCode401(exception)) {
                    Logger.i(TAG, "loginError tfa required");
                    fragment.onTfaFailure();
                } else {
                    fragment.onLoginFailure();
                }
            } else {
                fragment.onLoginFailure();
            }
            loginObservable = null;
        }

        private boolean errorCode401(HttpException exception) {
            return 401 == exception.code();
        }

        private boolean tfaRequired(String tfaHeader) {
            return tfaHeader != null && tfaHeader.contains("required");
        }

        private String getTfaHeader(HttpException throwable) {
            Headers headers = throwable.response().headers();
            return headers.get("X-GitHub-OTP");
        }

        private boolean retrofitError(Throwable throwable) {
            return throwable instanceof HttpException;
        }
    };
}
