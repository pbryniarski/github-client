package bryn.com.githubclient.presenter;

import java.io.IOException;

import javax.inject.Inject;

import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.fragment.RepositoryDetailFragment;
import bryn.com.githubclient.interactor.starring.RepoStarredInteractor;
import bryn.com.githubclient.utilities.Logger;
import rx.Subscription;
import rx.functions.Action1;

public class RepoDetailPresenter {

    private static final String TAG = "RepoDetailsPresenter";
    private final RepoStarredInteractor starInteractor;

    private RepositoryDetailFragment fragment;

    @Inject
    public RepoDetailPresenter(RepoStarredInteractor starInteractor) {
        this.starInteractor = starInteractor;
    }

    public void setFragment(RepositoryDetailFragment fragment) {
        this.fragment = fragment;
    }

    public Subscription getStarState(Repository repository) {
        return starInteractor.isRepoStarred(repository)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean starred) {
                        if (starred) {
                            fragment.setStarSelected();
                        } else {
                            fragment.setStarUnselected();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        fragment.setStarUnselected();
                        if (throwable instanceof IOException) {
                            fragment.showNetworkErrorInfo();
                        }
                    }
                });
    }

    public void starRepo(Repository repository, boolean star) {
        starInteractor.setRepoStarred(star, repository)
                .subscribe(new Action1<Void>() {
                               @Override
                               public void call(Void aVoid) {
                                   Logger.d(TAG, "repo starred");
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   Logger.e(TAG, "repo star failed", throwable);
                               }
                           }
                );
    }
}
