package bryn.com.githubclient.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;

import bryn.com.githubclient.NavigationController;
import bryn.com.githubclient.adapter.RepositoriesAdapter;
import bryn.com.githubclient.dataModel.RepoSearchResponse;
import bryn.com.githubclient.dataModel.Repository;
import bryn.com.githubclient.fragment.SearchFragment;
import bryn.com.githubclient.interactor.repositories.SearchReposInteractor;
import bryn.com.githubclient.persistence.db.SearchRepositoriesStorage;
import bryn.com.githubclient.persistence.file.RepoSearchWordsStorage;
import bryn.com.githubclient.utilities.Logger;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by pawelbryniarski on 13.12.15.
 */
public class RepoSearchPresenter implements RepositoriesAdapter.OnRepoClickedListener {

    private static final String TAG = "RepoSearchPresenter";
    private static final String LAST_DOWNLOADED_PAGE = "LAST_DOWNLOADED_PAGE";
    private static final String KEYWORD = "KEYWORD";
    private static final String MAX_PAGE = "MAX_PAGE";
    private final SearchReposInteractor interactor;
    private final NavigationController navigationController;
    private final RepoSearchWordsStorage wordsStorage;
    private final SharedPreferencesUtil sharedPreferences;
    private final SearchRepositoriesStorage searchRepositoriesStorage;

    private SearchFragment fragment;
    //for natural ordering
    private Set<String> searchHistory = new TreeSet<>();

    private String searchWord;
    private int lastDownloadPage;
    private int maxPage = 1;

    @Inject
    public RepoSearchPresenter(SearchReposInteractor interactor,
                               NavigationController navigationController,
                               RepoSearchWordsStorage wordsStorage,
                               SharedPreferencesUtil sharedPreferences,
                               SearchRepositoriesStorage searchRepositoriesStorage) {
        this.interactor = interactor;
        this.navigationController = navigationController;
        this.wordsStorage = wordsStorage;
        this.sharedPreferences = sharedPreferences;
        this.searchRepositoriesStorage = searchRepositoriesStorage;
    }

    public Subscription getFirstPage(String searchWord) {
        this.searchWord = searchWord;
        lastDownloadPage = 0;
        maxPage = 1;
        Logger.d(TAG, "getNextPage, word changed");
        if (!searchHistory.contains(searchWord)) {
            searchHistory.add(searchWord);
            wordsStorage.addWordAsync(searchWord);
            fragment.refreshSearchHistory(searchHistory);
        }
        return getNextPage();
    }

    @Nullable
    public Subscription getNextPage() {
        if (TextUtils.isEmpty(searchWord)) {
            return null;
        }

        if (maxPage == -1) {
            Logger.d(TAG, "getNextPage, no more pages");
            return null;
        }
        Logger.d(TAG, "getNextPage, will get page number: " + (lastDownloadPage + 1));
        fragment.showProgress();
        return interactor.search(searchWord, lastDownloadPage + 1)
                .subscribe(new Action1<RepoSearchResponse>() {
                    @Override
                    public void call(RepoSearchResponse repoSearchResponse) {
                        Logger.d(TAG, "New repos delivered");

                        maxPage = repoSearchResponse.maxPage;
                        if (lastDownloadPage == 0) {
                            fragment.clearRepos();
                        }
                        fragment.onNewRepositories(Arrays.asList(repoSearchResponse.items));
                        lastDownloadPage++;
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Logger.e(TAG, "Getting repos failed:", throwable);
                        fragment.hideProgress();
                        fragment.showRepoFetchError();
                    }
                }, new Action0() {
                    @Override
                    public void call() {
                        Logger.d(TAG, "Getting repos completed");
                        fragment.hideProgress();

                    }
                });
    }

    public Subscription getSearchHistory() {
        return wordsStorage.readWordsAsync().subscribe(new Action1<List<String>>() {
            @Override
            public void call(List<String> strings) {
                searchHistory.addAll(strings);
                //will have it sorted
                fragment.setSearchHistory(new ArrayList<>(searchHistory));
            }
        });
    }

    public void setFragment(SearchFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onClicked(Repository repository) {
        navigationController.showRepoDetails(repository);
    }

    public void saveState(Bundle outState) {
        outState.putInt(LAST_DOWNLOADED_PAGE, lastDownloadPage);
        outState.putString(KEYWORD, searchWord);
        outState.putInt(MAX_PAGE, maxPage);
    }


    public void restoreState(Bundle inState) {
        lastDownloadPage = inState.getInt(LAST_DOWNLOADED_PAGE);
        searchWord = inState.getString(KEYWORD);
        maxPage = inState.getInt(MAX_PAGE);
    }

    public void saveLastSearch(List<Repository> repositories) {
        sharedPreferences.saveLastSearchWord(searchWord);
        sharedPreferences.saveLastMaxSearchPage(maxPage);
        sharedPreferences.saveLastSearchDownloadedPage(lastDownloadPage);
        Observable.just(repositories)
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<List<Repository>>() {
                    @Override
                    public void call(List<Repository> repositories) {
                        searchRepositoriesStorage.removeData();
                        searchRepositoriesStorage.saveRepositories(repositories);
                    }
                });
    }

    public Subscription restoreLastSearch() {
        this.searchWord = sharedPreferences.getLastSearchWord();
        fragment.setSearchWord(searchWord);
        maxPage = sharedPreferences.getLastMaxSearchPage();
        lastDownloadPage = sharedPreferences.getLastSearchDownloadedPage();
        return Observable.just(searchRepositoriesStorage.readRepositories())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Repository>>() {
                    @Override
                    public void call(List<Repository> repositories) {
                        fragment.onNewRepositories(repositories);
                    }
                });
    }
}
