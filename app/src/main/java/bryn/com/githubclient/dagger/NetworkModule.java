package bryn.com.githubclient.dagger;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import bryn.com.githubclient.BuildConfig;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.network.HeaderAdderInterceptor;
import bryn.com.githubclient.network.LoggingInterceptor;
import dagger.Module;
import dagger.Provides;
import retrofit.MoshiConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient getHttpClient(HeaderAdderInterceptor headerAdderInterceptor) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(headerAdderInterceptor);
        if (BuildConfig.DEBUG) {
            okHttpClient.interceptors().add(new LoggingInterceptor());
        }
        return okHttpClient;
    }

    @Provides
    @Singleton
    GithubApi getGithubApi(OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.github.com")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        return retrofit.create(GithubApi.class);
    }


}
