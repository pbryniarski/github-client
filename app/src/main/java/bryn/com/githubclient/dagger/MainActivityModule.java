package bryn.com.githubclient.dagger;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import javax.inject.Named;

import bryn.com.githubclient.R;
import bryn.com.githubclient.activity.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by pawelbryniarski on 08.12.15.
 */
@Module
public class MainActivityModule {

    private final Activity activity;
    private final Toolbar toolbar;
    private final ActionBar actionBar;

    public MainActivityModule(Activity activity, Toolbar toolbar, ActionBar actionBar) {
        this.activity = activity;
        this.toolbar = toolbar;
        this.actionBar = actionBar;
    }

    @Provides
    @MainActivityScope
    public Activity getActivity() {
        return activity;
    }

    @Provides
    @MainActivityScope
    public MainActivity getMainActivity() {
        return (MainActivity) activity;
    }

    @Provides
    @MainActivityScope
    public FragmentManager getFragmentManager(MainActivity activity) {
        return activity.getFragmentManager();
    }

    @Provides
    @MainActivityScope
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Provides
    @MainActivityScope
    public ActionBar getActionBar() {
        return actionBar;
    }

    @Provides
    @MainActivityScope
    public Picasso getPicasso(Activity activity) {
        return Picasso.with(activity);
    }

    @Provides
    @MainActivityScope
    public AutoCompleteTextView getSearchView(Toolbar toolbar) {
        return (AutoCompleteTextView) toolbar.findViewById(R.id.search_text);
    }

    @Provides
    @MainActivityScope
    public ImageButton getClearText(Toolbar toolbar) {
        return (ImageButton) toolbar.findViewById(R.id.clear_text);
    }

    @Provides
    @MainActivityScope
    @Named("search_bar")
    public View getSearchBar(Toolbar toolbar) {
        return  toolbar.findViewById(R.id.search_bar);
    }
}
