package bryn.com.githubclient.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by pawelbryniarski on 08.12.15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainActivityScope {
}