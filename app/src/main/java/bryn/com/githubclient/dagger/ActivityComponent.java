package bryn.com.githubclient.dagger;

import bryn.com.githubclient.activity.MainActivity;
import bryn.com.githubclient.fragment.LoginFragment;
import bryn.com.githubclient.fragment.RepositoryDetailFragment;
import bryn.com.githubclient.fragment.SearchFragment;
import bryn.com.githubclient.fragment.UserRepositoriesFragment;
import dagger.Component;

/**
 * Created by pawelbryniarski on 08.12.15.
 */
@MainActivityScope
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);

    void inject(LoginFragment loginFragment);

    void inject(UserRepositoriesFragment userRepositoriesFragment);

    void inject(SearchFragment searchFragment);

    void inject(RepositoryDetailFragment repositoryDetailFragment);

}
