package bryn.com.githubclient.dagger;

import android.content.Context;

import javax.inject.Singleton;

import bryn.com.githubclient.interactor.repositories.UserRepositoriesInteractor;
import bryn.com.githubclient.interactor.starring.RepoStarredInteractor;
import bryn.com.githubclient.interactor.starring.StarRequestSyncService;
import bryn.com.githubclient.network.GithubApi;
import bryn.com.githubclient.persistence.file.RepoStarRequestStorage;
import bryn.com.githubclient.persistence.db.SearchRepositoriesStorage;
import bryn.com.githubclient.persistence.db.UserRepositoriesStorage;
import bryn.com.githubclient.utilities.SchedulersFactory;
import bryn.com.githubclient.utilities.SharedPreferencesUtil;
import bryn.com.githubclient.utilities.Utils;
import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, AppModule.class})
public interface AppComponent {
    Utils getUtilities();

    GithubApi getGithubApi();

    SharedPreferencesUtil geSharedPreferencesUtil();

    UserRepositoriesInteractor getUserRepositoriesInteractor();

    Context getApplicationContext();

    RepoStarredInteractor getRepoStarredInteractor();

    UserRepositoriesStorage getUserRepositoriesStorage();

    SearchRepositoriesStorage getSearchRepositoriesStorage();

    RepoStarRequestStorage getRepoStarRequestStorage();

    void inject(StarRequestSyncService starRequestSyncService);

    SchedulersFactory getSchedulersFactory();
}
