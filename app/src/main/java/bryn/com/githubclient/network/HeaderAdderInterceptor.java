package bryn.com.githubclient.network;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by pawelbryniarski on 08.12.15.
 */
public class HeaderAdderInterceptor implements Interceptor {

    @Inject
    public HeaderAdderInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest = request.newBuilder()
                .addHeader("Accept", "application/vnd.github.v3+json")
                .build();
        return chain.proceed(newRequest);
    }
}
