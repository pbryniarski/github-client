package bryn.com.githubclient.network;

import com.squareup.okhttp.ResponseBody;

import bryn.com.githubclient.dataModel.RepoSearchResponse;
import bryn.com.githubclient.dataModel.Repository;
import retrofit.Call;
import retrofit.Response;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface    GithubApi {

    @GET("/")
    Observable<Void> login(@Header("Authorization") String basicAuthenticationCredentials);

    @GET("/")
    Observable<Void> login(@Header("Authorization") String basicAuthenticationCredentials,
                           @Header("X-GitHub-OTP") String token);

    @GET("/user/repos")
    Observable<Response<Repository[]>> getUserRepos(@Header("Authorization") String basicAuthenticationCredentials,
                                                    @Query("page") int page);

    @GET("/user/repos")
    Call<Repository[]> getUserReposSynchronous(@Header("Authorization") String basicAuthenticationCredentials,
                                               @Query("page") int page);

    @GET("search/repositories")
    Observable<Response<RepoSearchResponse>> search(@Query("q") String keyword, @Query("page") int page);

    @GET("user/subscriptions/{fullRepoName}")
    Call<ResponseBody> isWatched(@Header("Authorization") String basicAuthenticationCredentials,
                                 @Path("fullRepoName") String fullRepositoryName);

    @GET("user/starred/{owner}/{repoName}")
    Observable<Response<Void>> isStarred(@Header("Authorization") String basicAuthenticationCredentials,
                                         @Path("owner") String owner,
                                         @Path("repoName") String repoName);

    @Headers("Content-Length: 0")
    @PUT("user/starred/{owner}/{repoName}")
    Observable<Response<Void>> starRepo(@Header("Authorization") String basicAuthenticationCredentials,
                                        @Path("owner") String owner,
                                        @Path("repoName") String repoName);


    @Headers("Content-Length: 0")
    @DELETE("user/starred/{owner}/{repoName}")
    Observable<Response<Void>> unstarRepo(@Header("Authorization") String basicAuthenticationCredentials,
                                          @Path("owner") String owner,
                                          @Path("repoName") String repoName);

    @Headers("Content-Length: 0")
    @PUT("user/starred/{owner}/{repoName}")
    Call<ResponseBody> starRepoSync(@Header("Authorization") String basicAuthenticationCredentials,
                                    @Path("owner") String owner,
                                    @Path("repoName") String repoName);


    @Headers("Content-Length: 0")
    @DELETE("user/starred/{owner}/{repoName}")
    Call<ResponseBody> unstarRepoSync(@Header("Authorization") String basicAuthenticationCredentials,
                                      @Path("owner") String owner,
                                      @Path("repoName") String repoName);
}
