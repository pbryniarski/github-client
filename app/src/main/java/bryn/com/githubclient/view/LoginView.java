package bryn.com.githubclient.view;

public interface LoginView {
    void onLoginSuccess();

    void onLoginFailure();

    void onTfaFailure();

    void onLoginStart();

    void onLoginStop();
}
